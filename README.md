# Description

Software to create RD53B-like data streams from Monte Carlo events and calculate respective data rates.
It is currently implemented with studies for TEPX in mind.

*This is a development version and does not ensure any correctness.*

The framework takes as input a (for now) CSV file with the information of a pixel hit, one CSV file per ring of a disk.
The file name gives information about the ring and disk number as well as the pileup scenario.
One line has following format (might be refined):
```
event number, module number, row, column, adc value
```

The CSV file is generated from CMSSW samples using the [ITdigiExporter](https://gitlab.cern.ch/aruede/itdigiexporter).
For further information about the (BRIL) simulation take a look at [The not yet ultimate guide to BRIL Inner Tracker Simulations](https://github.com/gauzinge/BRIL_ITsim).

<!-- The current implementation builds one stream per event per module, without making use of the 'neighbour' or 'more bits' reduction mechanisms (to be implemented).
This is not exactly how it is done in reality, but will deliver a first approximation.
Streams have to be implemented per ROC of a module. -->

The encoding uses the 2x8 quarter core scheme with isLast and isNeigbor bits.
For now one stream contains one full event of a whole module.

# Directory Structure

+-- digis (Exported digis from CMSSW)

+-- it_encoder (Code for encoding and stream building)

+-- plot_scripts (Scripts for analyzing and plotting data)

+-- rates (Calculated stream lengths per event)


# Usage

Example: Calculate average stream length for Disk 4 Ring 1 for PU 200 events over 8 events.
```
cd it_encoder
python data_rate.py ../digis/Digis_Disk4_Ring1_PU_200.0.0.csv 8 2x8
```

Then look at the numbers and plot:
```
cd  plot_scripts
python ring_rates_bp.py ../rates/
```

Using Python 3(.6)
