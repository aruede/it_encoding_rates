import numpy as np
import csv
import sys
sys.path.insert(0, '../it_encoder')
import tepx

def get_disk_nb_int( filename ):
  """ Return disk number string from file name. """
  disk_nb = filename[filename.find("Disk"):filename.find("Disk")+5]
  return int(disk_nb[-1])

def get_ring_nb( filename ):
  """ Return ring number string to use with tepx dict. """
  ring_nb = filename[filename.find("Ring"):filename.find("Ring")+5]
  return ring_nb

def get_ring_nb_int( filename ):
  """ Return ring number string to use with tepx dict. """
  ring_nb = filename[filename.find("Ring"):filename.find("Ring")+5]
  return int(ring_nb[-1])

def get_n_modules( ring_nb ):
  """ Return number of modules for specified ring. """
  n_modules = tepx.ringDict[ring_nb]
  return n_modules

def read_ring_stream_len( filename ):
  """ Return 
  - array with average stream length per module in ring
  - average stream length in ring
  - std dev for ring
  """
  n_modules = get_n_modules( get_ring_nb( filename ) )
  module_data_len = np.zeros( n_modules )
  event_cnt = 0
  with open( filename ) as f:
    for line in csv.reader( f ):
      for i, mod in enumerate( line[:n_modules] ):
        module_data_len[i] += int(mod)
      event_cnt += 1
  module_data_len /= event_cnt
  return module_data_len, np.mean(module_data_len), np.std(module_data_len)

def read_ring_rates( filename, trig_rate ):
  """ Return list with all rates of that ring """
  ring_lens = []
  event_cnt = 0
  with open( filename ) as f:
    for line in csv.reader( f ):
      for mod in line[:-1]:
        ring_lens.append( int(mod) * trig_rate/1000000 )
      event_cnt += 1
  return ring_lens, event_cnt

def read_ring_lens( filename ):
  """ Return list with all rates of that ring """
  ring_lens = []
  event_cnt = 0
  with open( filename ) as f:
    for line in csv.reader( f ):
      for mod in line[:-1]:
        ring_lens.append( int(mod) )
      event_cnt += 1
  return ring_lens, event_cnt

def boxplot_style( bp, fillcolor, mediancolor ):
  ## change outline color, fill color and linewidth of the boxes
  for box in bp['boxes']:
      # change outline color
      box.set( color='#000000', linewidth=1.5)
      # change fill color
      box.set( facecolor = fillcolor )
  ## change color and linewidth of the whiskers
  for whisker in bp['whiskers']:
      whisker.set(color='#000000', linewidth=1.5)
  ## change color and linewidth of the caps
  for cap in bp['caps']:
      cap.set(color='#000000', linewidth=1.5)
  ## change color and linewidth of the medians
  for median in bp['medians']:
      median.set(color=mediancolor, linewidth=3)
  ## change the style of fliers and their fill
  for flier in bp['fliers']:
      flier.set(marker='o', alpha=0.0)
