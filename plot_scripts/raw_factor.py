import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import analyzer as ana

path = sys.argv[1]
# trigger rate in kHz
trig_rate = 750

def rings_dict():
    rings = { 1 : [], 2 : [], 3 : [], 4 : [], 5 : [] }
    return rings

disks_enc = { 1 : rings_dict(), 2 : rings_dict(), 3 : rings_dict(), 4 : rings_dict() }
disks_raw = { 1 : rings_dict(), 2 : rings_dict(), 3 : rings_dict(), 4 : rings_dict() }

"""
Reading encoded data stream lengths
"""
for filename in sorted( os.listdir( path ) ):
    if "datalength" in filename and "raw" not in filename and "notot" not in filename:
        print( filename )
        ring_lens, event_cnt = ana.read_ring_lens( path + filename, trig_rate )
        disk_nb = ana.get_disk_nb_int( filename )
        ring_nb = ana.get_ring_nb_int( filename )
        disks_enc[disk_nb][ring_nb].extend( ring_lens )

"""
Reading raw data stream lengths
"""
for filename in sorted( os.listdir( path ) ):
    if "datalength" in filename and "raw" in filename and "notot" not in filename:
        print( filename )
        ring_lens, event_cnt = ana.read_ring_lens( path + filename, trig_rate )
        disk_nb = ana.get_disk_nb_int( filename )
        ring_nb = ana.get_ring_nb_int( filename )
        disks_raw[disk_nb][ring_nb].extend( ring_lens )

all_disks_enc = rings_dict()
all_disks_raw = rings_dict()
for disk in [1,2,3,4]:
    for ring in [1,2,3,4,5]:
        enc_mean =  sum( disks_enc[disk][ring] ) / len( disks_enc[disk][ring] )
        raw_mean =  sum( disks_raw[disk][ring] ) / len( disks_raw[disk][ring] )
        reduction = raw_mean / enc_mean
        # print("---")
        # print( "Enc mean of Disk " + str(disk) + " Ring " + str(ring) + " : " + str(enc_mean/4) )
        # print( "Raw mean of Disk " + str(disk) + " Ring " + str(ring) + " : " + str(raw_mean/4) )
        # print( "Reduction of Disk " + str(disk) + " Ring " + str(ring) + " : " + str(reduction) )
        all_disks_enc[ring].append( enc_mean )
        all_disks_raw[ring].append( raw_mean )

for ring in [1,2,3,4,5]:
        enc_mean_all =  sum( all_disks_enc[ring] ) / len( all_disks_enc[ring] )
        raw_mean_all =  sum( all_disks_raw[ring] ) / len( all_disks_raw[ring] )
        reduction = raw_mean_all / enc_mean_all
        print("---")
        print( "Raw mean of Ring " + str(ring) + " : " + str(raw_mean_all/4) )
        print( "Enc mean of Ring " + str(ring) + " : " + str(enc_mean_all/4) )
        print( "Reduction of Ring " + str(ring) + " : " + str(reduction) )
