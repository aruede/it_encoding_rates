import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import csv
import analyzer as ana

path = sys.argv[1]

# trigger rate in kHz
trig_rate = 750

def rings_dict():
    rings = { 1 : [], 2 : [], 3 : [], 4 : [], 5 : [] }
    return rings

""" Dictionary of one TEPX side """
disks = { 1 : rings_dict(), 2 : rings_dict(), 3 : rings_dict(), 4 : rings_dict() }

"""
Open every file in specified directory (every ring of every disk)
and read the lists of rates into the TEPX dictionary.
"""
for filename in sorted( os.listdir( path ) ):
    if "datalength" in filename and "raw" not in filename and not "notot" in filename and filename.endswith(".csv"):
        print( filename )
        # ring_lens, event_cnt = ana.read_ring_rates( path + filename, trig_rate )
        ring_lens, event_cnt = ana.read_ring_lens( path + filename )
        disk_nb = ana.get_disk_nb_int( filename )
        ring_nb = ana.get_ring_nb_int( filename )
        disks[disk_nb][ring_nb].extend( ring_lens )

all_rings = rings_dict()

for disk in disks:
    for ring in disks[disk]:
        all_rings[ring].extend( disks[disk][ring] )

data_to_plot = [ all_rings[ring] for ring in all_rings ]


#####################################
## PLOT
#####################################

fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)

fillcolors = ['#fcb76f', '#e88666', '#c05166', '#8f3066', '#541d61']
medianc = '#51c0ab'
for i, ring in enumerate(all_rings):
  bp = ax.boxplot( all_rings[ring], patch_artist=True, showfliers=False, positions=[i+1] )
  ana.boxplot_style( bp, fillcolors[i], mediancolor=medianc )
  medians = [ med.get_ydata()[0] for med in bp['medians'] ]
  print("Ring " + str(ring) )
  print(medians[0]/4)

## Set labels
ax.set_xticklabels(['Ring 1', 'Ring 2', 'Ring 3', 'Ring 4', 'Ring 5'])
ax.tick_params(axis="x", labelsize=14)
ax.tick_params(axis="y", labelsize=12)

### Custom lines and ticks ###
# medians = [ med.get_ydata()[0] for med in bp['medians'] ]
# rings = [1, 2, 3, 4, 5]
# plt.yticks( medians )
# ax.hlines(medians, [0,0,0,0,0], color='k', xmax=rings, linestyles='dashed', linewidth=1)


ax.set_ylabel('Module Data Rate [Gbps]', fontsize=14,labelpad=15)

ax.yaxis.grid()
plt.xlim( 0.5, 5.5 )
plt.ylim( 0 )
plt.show()
