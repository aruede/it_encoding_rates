import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import sys
import os
import analyzer as ana

path = sys.argv[1]
# trigger rate in kHz
trig_rate = 750

def rings_dict():
    rings = { 1 : [], 2 : [], 3 : [], 4 : [], 5 : [] }
    return rings

disks = { 1 : rings_dict(), 2 : rings_dict(), 3 : rings_dict(), 4 : rings_dict() }

"""
Open every file in specified directory (every ring of every disk)
and read the lists of rates into the TEPX dictionary.
"""
for filename in sorted( os.listdir( path ) ):
    if "datalength" in filename and "raw" not in filename and not "notot" in filename and filename.endswith(".csv"):
        print( filename )
        ring_lens, event_cnt = ana.read_ring_lens( path + filename, trig_rate )
        disk_nb = ana.get_disk_nb_int( filename )
        ring_nb = ana.get_ring_nb_int( filename )
        disks[disk_nb][ring_nb].extend( ring_lens )

#####################################
## PLOT
#####################################

def tick_text(ax, pos=[0,0], scalex=1, scaley=1, text="",textkw = {}, linekw = {}):
    x = np.array([0, 0.05, 0.45,0.5])
    y = np.array([0,-0.01,-0.01,-0.02])
    x = np.concatenate((x,x+0.5)) 
    y = np.concatenate((y,y[::-1]))
    ax.text(pos[0]+0.5*scalex, (y.min()-0.01)*scaley+pos[1], text, 
                transform=ax.get_xaxis_transform(),
                ha="center", va="top", **textkw, fontsize=14)

fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)

# Kian color palette
colors = [ '#96384e', '#04354b', '#0297a0', '#037665' ]
medianc='#eed284'
for i, disk in enumerate(disks):
  bp = ax.boxplot([ disks[disk][ring] for ring in disks[disk] ], patch_artist=True, showfliers=False, positions=[0+i,5+i,10+i,15+i,20+i] )
  ana.boxplot_style( bp, fillcolor=colors[i], mediancolor=medianc )
  medians = [ med.get_ydata()[0] for med in bp['medians'] ]
  print("Disk " + str(disk) )
  print(medians)

## Legend
patches = []
for i, c in enumerate( colors ):
  patches.append( mpatches.Patch( facecolor=c, label='Disk '+str(i+1), edgecolor='k', linewidth=1.5 ) )
plt.legend(handles=patches, fontsize=14)

## Set labels
ax.set_xticklabels([''])
ax.tick_params(axis="x", labelsize=14)
ax.tick_params(axis="y", labelsize=12)

# ax1.set_xlabel('Rings and disks')
ax.set_ylabel('Module Data Rate [Gbps]', fontsize=14, labelpad=15)

## tick_text
tick_text(ax, text="Ring 1", pos=[-0.5,0], linekw=dict(color="black", lw=1.2), scalex=4, scaley=1 )
tick_text(ax, text="Ring 2", pos=[4.5,0], linekw=dict(color="black", lw=1.2), scalex=4, scaley=1 )
tick_text(ax, text="Ring 3", pos=[9.5,0], linekw=dict(color="black", lw=1.2), scalex=4, scaley=1 )
tick_text(ax, text="Ring 4", pos=[14.5,0], linekw=dict(color="black", lw=1.2), scalex=4, scaley=1 )
tick_text(ax, text="Ring 5", pos=[19.5,0], linekw=dict(color="black", lw=1.2), scalex=4, scaley=1 )

ax.yaxis.grid()
plt.ylim( 0 )
plt.show()
