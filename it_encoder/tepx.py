#############################################################
# Author: Alexander Ruede (CERN/KIT-IPE)
# Description:  - TEPX topology and module information
#############################################################

import numpy as np
import matplotlib.pyplot as plt

"""
Dictionary that holds the number of modules per ring
Key: String of ring number (to be read from file name)
Value: number of quad modules
"""
ringDict = { "Ring1" : 20, "Ring2" : 28, "Ring3" : 36, "Ring4" : 44, "Ring5" : 48 }

class roc():
  """
  Describing a CMS ROC with 25um x 100um pixels.
  - nrows: Total number of pixel rows in chip
  - ncols: Total number of pixel columns in chip
  - matrix: Pixel matrix (numpy array of int8)
  """
  def __init__( self ):
    self.nrows = 656
    self.ncols = 221
    self.matrix = np.zeros( (self.nrows, self.ncols), dtype=np.int8 )

class module():
  """
  Describing a TEPX quad module (2x2 RD53B ROCs) with 25x100 pixels.
  - nrows: Total number of pixel rows in module
  - ncols: Total number of pixel columns in module
  - nccols: Number of core columns in module
  - qcrows: Number of quarter core rows in modules (number of 2x8 region rows)
  - matrix: Pixel matrix (numpy array of int8)
  REMARK: In CMSSW there is a small gap between the ROCs of a module which leads
  to an increased number of rows of a module.
  """
  def __init__( self ):
    self.nrows = 1320 + 40 # to be evaluated
    self.ncols = 442
    self.nccols = self.ncols//8
    self.qcrows = self.nrows//2
    self.matrix = np.zeros( (self.nrows, self.ncols), dtype=np.uint8 )
    self.last_qrow_pos = np.zeros( self.nccols, dtype=np.uint8 )

class module_5050():
  """
  Describing a TEPX quad module (2x2 RD53B ROCs) with 50x50 pixels.
  - nrows: Total number of pixel rows in module
  - ncols: Total number of pixel columns in module
  - nccols: Number of core columns in module
  - qcrows: Number of quarter core rows in modules (number of 2x8 region rows)
  - matrix: Pixel matrix (numpy array of int8)
  REMARK: In CMSSW there is a small gap between the ROCs of a module which leads
  to an increased number of rows of a module.
  """
  def __init__( self ):
    self.nrows = (1320 + 40) // 2
    self.ncols = 442 * 2
    self.nccols = self.ncols//8
    self.qcrows = self.nrows//2
    self.matrix = np.zeros( (self.nrows, self.ncols), dtype=np.uint8 )
    self.last_qrow_pos = np.zeros( self.nccols, dtype=np.uint8 )

def transform_module( module ):
  """
  Remapping of the sensor pixels to the ROC
  from 25x200 to 50x50 module
  """
  new_module = module_5050()
  for row in range( module.nrows ):
    for col in range( module.ncols ):
      if row % 2 == 0:
        new_module.matrix[ row//2, col*2 ] = module.matrix[ row, col ]
      else:
        new_module.matrix[ row//2, (col*2)+1 ] = module.matrix[ row, col ]
  return new_module

import csv

def read_ring_event( filename, event_nb, convert=False ):
  """
  Reading one event of a ring into a list of modules,
  containing the pixel matrix with hit information.
  There should be one .csv file for each ring,
  containing all events, one line per fired pixel with following format:
  event number, module number, row, column, tot value
  """
  # Get ring number from filename
  ring_nb = filename[filename.find("Ring"):filename.find("Ring")+5]
  print("Reading event " + str(event_nb) + ", " + ring_nb)
  # Create list with one module() for each module in that ring
  event = [ module() for i in range(ringDict[ring_nb]) ]
  with open( filename ) as f:
    for line in csv.reader( f ):
      if int(line[0]) < event_nb:
        # Ignore events that do not match the target event number
        continue
      elif int(line[0]) == event_nb:
        if convert:
          # Very basic conversion from charge to 4 bit value
          # Only needed for ClusterExporter, not for DigiExporter
          conv = int(line[4]) / 600
          tot_value = conv if conv < 16 else 15
          # Fill each module's pixel matrix
          event[int(line[1])-1].matrix[int(line[2])][int(line[3])] = tot_value
        else:
          event[int(line[1])-1].matrix[int(line[2])][int(line[3])] = int(line[4])
      else:
        # Return event when finished
        return event

 


def plot_module( module_obj ):
  """
  Plot pixel matrix
  TODO: matplotlib on p3.6 not working on lxplus?
  """
  plt.imshow( module_obj.matrix, interpolation='nearest' )
  plt.show()
