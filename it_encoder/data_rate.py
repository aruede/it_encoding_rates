import numpy as np
import csv
import sys
import encoder as enc
import tepx

if len(sys.argv) < 4:
  print("Wrong number of arguments!")
  print("Usage: data_rate.py <digis/filename> <n_events> <2x8/4x4> (<notot>)")
  sys.exit()

filename = sys.argv[1]
n_events = int(sys.argv[2])
qcsize = sys.argv[3]
if len(sys.argv) == 5:
  notot = sys.argv[4]
else:
  notot = None

# Select the shape of a quarter core
if qcsize == "2x8":
  row_factor = 2
  col_factor = 8
elif qcsize == "4x4":
  row_factor = 4
  col_factor = 4
  raise ValueError("4x4 quarter cores not implemented yet!")


# Tag has fixed length of 8 (if at beginning of stream)
tag_len = 8
# The addres length is now assumed to be:
# 8 b quartercore row + 6 b core column + 2 b isneighbour & islast
qrow_addr_len = 8
ccol_addr_len = 6
aux_bit_len = 2

"""
This writes a file with the basic stream length of each module for one event per line.
"""
if notot == "notot":
  enc_filename = filename.strip(".csv") + "_datalength_notot_" + str(n_events) + "_5050.csv"
  raw_filename = filename.strip(".csv") + "_raw_datalength_notot_" + str(n_events) + "_5050.csv"
else:
  enc_filename = filename.strip(".csv") + "_datalength_" + str(n_events) + "_5050.csv"
  raw_filename = filename.strip(".csv") + "_raw_datalength_" + str(n_events) + "_5050.csv"


with open( enc_filename, 'w' ) as f_enc, open( raw_filename, 'w' ) as f_raw:
  # For every event
  for evntnb in range(n_events):
    # Read the event hit data into each module
    ring_event_25x100 = tepx.read_ring_event( filename, evntnb )
    ### REMAPPING
    ring_event = [ tepx.transform_module( module ) for module in ring_event_25x100 ]
    del ring_event_25x100[:]
    ###
    ring_nb = filename[filename.find("Ring"):filename.find("Ring")+5]
    module_data_len = [0] * tepx.ringDict[ring_nb]
    raw_module_data_len = [0] * tepx.ringDict[ring_nb]
    # Loop over every module of the ring
    for module_nb, tepx_module in enumerate( ring_event ):
      stream_len = 0
      # Raw event size
      raw_module_data_len[module_nb] = enc.raw_bits( tepx_module.matrix, notot )
      # Loop over each core column
      for ccol in range( tepx_module.ncols//col_factor ):
        qcrow_prev = -2
        islast = False
        isneighbour = False
        # Find position of last qcrow in current ccol with a hit (for islast bit)
        ccol_data = tepx_module.matrix[:, ccol*col_factor:(ccol+1)*col_factor]
        last_qcrow = enc.find_last_qrow( ccol_data )
        if last_qcrow is not None:
          # Loop over each quarter core row
          for qcrow in range ( last_qcrow+1 ):
            qcore_arr = np.zeros(16)
            qcore_arr[:col_factor] = tepx_module.matrix[qcrow*row_factor][ccol*col_factor:(ccol+1)*col_factor]
            qcore_arr[col_factor:] = tepx_module.matrix[qcrow*row_factor+1][ccol*col_factor:(ccol+1)*col_factor]
            # Next iteration if no hits in this qcore
            if np.count_nonzero( qcore_arr ) == 0:
              continue
            else:
              """
              -- ENCODING --
              Encode the quarter core and get the data lengths.
              """
              bte_data_len, ToT_len, n_hits_len = enc.qcore_len( qcore_arr )
              """
              -- ISLAST --
              """
              islast = enc.islast( qcrow, last_qcrow )
              """
              -- ISNEIGHBOR --
              """
              isneighbour = enc.isneighbour( qcrow_prev, qcrow )
              qcrow_prev = qcrow
              """
              Build the stream length
              """
              # Check if ToT info suppressed
              if notot and notot == "notot":
                ToT_len = 0
              # Omit the qrow address if isneighbour bit is assigned
              if not isneighbour:
                stream_len += qrow_addr_len + aux_bit_len + bte_data_len + ToT_len
              else:
                stream_len += aux_bit_len + bte_data_len + ToT_len
              # The event stream contains as many ccol addresses as there are islast bits (correct?)
              if islast:
                stream_len += ccol_addr_len

      module_data_len[module_nb] = tag_len + stream_len
    
    # Write raw data size
    for module in raw_module_data_len:
          f_raw.write( str(module) + ",")
    f_raw.write( "\n")
    # Write encoded data size
    for module in module_data_len:
      f_enc.write( str(module) + ",")
    f_enc.write( "\n")
