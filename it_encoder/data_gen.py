import numpy as np
import csv
import sys
import encoder as enc
import tepx

if len(sys.argv) < 4:
  print("Wrong number of arguments!")
  print("Usage: data_rate.py <digis/filename> <n_events> <2x8/4x4>")
  sys.exit()

filename = sys.argv[1]
n_events = int(sys.argv[2])
qcsize = sys.argv[3]
if len(sys.argv) == 5:
  gen_raw = sys.argv[4]
else:
  gen_raw = None

# Select the shape of a quarter core
if qcsize == "2x8":
  row_factor = 2
  col_factor = 8
elif qcsize == "4x4":
  row_factor = 4
  col_factor = 4
  raise ValueError("4x4 quarter cores not implemented yet!")


# Tag has fixed length of 8 (if at beginning of stream)
tag_len = 8
# The addres length is now assumed to be:
# 8 b quartercore row + 6 b core column + 2 b isneighbour & islast
qrow_addr_len = 8
ccol_addr_len = 6
aux_bit_len = 2

"""
This writes a file with the basic stream length of each module for one event per line.
"""
with open( filename.strip(".csv") + "_datalength_" + str(n_events) + ".csv", 'w' ) as fo:
  # For every event
  for evntnb in range(n_events):
    # Read the event hit data into each module
    ring_event = tepx.read_ring_event( filename, evntnb )
    ### REMAPPING
    ring_event = [ tepx.transform_module( module ) for module in ring_event ]
    ###
    ring_nb = filename[filename.find("Ring"):filename.find("Ring")+5]
    module_data_len = [0] * tepx.ringDict[ring_nb]
    # Loop over every module of the ring
    for module_nb, tepx_module in enumerate( ring_event ):
      stream_len = 0
      # Loop over each core column
      for ccol in range( tepx_module.ncols//col_factor ):
        qcrow_prev = -2
        islast = False
        isneighbour = False
        # Find position of last qcrow in current ccol with a hit (for islast bit)
        ccol_data = tepx_module.matrix[:, ccol*col_factor:(ccol+1)*col_factor]
        last_qcrow = enc.find_last_qrow( ccol_data )
        # if a last quarter core row exists means there is data in that column
        if last_qcrow is not None:
          # Loop over each quarter core row
          for qcrow in range ( last_qcrow+1 ):
            qcore_arr = np.zeros(16)
            qcore_arr[:col_factor] = tepx_module.matrix[qcrow*row_factor][ccol*col_factor:(ccol+1)*col_factor]
            qcore_arr[col_factor:] = tepx_module.matrix[qcrow*row_factor+1][ccol*col_factor:(ccol+1)*col_factor]
            # Next iteration if no hits in this qcore
            if np.count_nonzero( qcore_arr ) == 0:
              continue
            else:
              """
              -- ENCODING --
              Encode the quarter core and get the data
              """
              bte_data, ToT, n_hits = enc.qcore_encoding( qcore_arr )
              """
              -- ISLAST --
              """
              islast = enc.islast( qcrow, last_qcrow )
              """
              -- ISNEIGHBOR --
              """
              isneighbour = enc.isneighbour( qcrow_prev, qcrow )
              qcrow_prev = qcrow
              """
              Build the stream
              """
              # Omit the qrow address if isneighbour bit is assigned
              if not isneighbour:
                stream_len += qrow_addr_len + isneighbour + islast + bte_data + ToT
              else:
                stream_len += isneighbour + islast + bte_data + ToT
              # The event stream contains as many ccol addresses as there are islast bits (correct?)
              if islast:
                stream_len += ccol_addr_len

      module_data_len[module_nb] = tag_len + stream_len
    
    for module in module_data_len:
      fo.write( str(module) + ",")
    fo.write( "\n")
